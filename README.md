# Conalt
> Landing Page desenvolvida por: @NapaTech

```
# Clonar o Projeto
$ git clone conalt.git
# Entrar na pasta e instalar as dependências necessárias
$ cd conalt && yarn
# Arquivo de config
$ cp .env.example .env
# Informações do Whatsapp
# Contato
# Envio de Mensagem
# Edite o arquivo .env
# -------------------------
# Modo de Desenvolvimento
$ yarn start # DEV MODE
#
# Modo de Produção
$ yarn build # PRODUCTION MODE
#
# Para testar modo de produção
$ yarn global add serve
$ serve -s build
```

Como configurar corretamente a rota:


```javascript
// file: ./src/routes.js
export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={App} />
        <Redirect from="*" to="/" />
      </Switch>
    </BrowserRouter>
  );
}
```

```javascript
// file: package.json
{
  ...
  "homepage": "https://conalt.com.br",
  ...
}
```
