<?php
header("Access-Control-Allow-Origin: *");
$rest_json = file_get_contents("php://input");

$_POST = json_decode($rest_json, true);

if ($_POST) {
  http_response_code(200);
  require '../phpmailer/PHPMailerAutoload.php';

  try {
    $mail = new PHPMailer(true);
    // $mail->SMTPDebug = 3;                               // Enable verbose debug output
    $mail->isSMTP();
    $mail->CharSet = "UTF-8";// Set mailer to use SMTP
    $mail->Host = 'mail.conalt.com.br';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'sendmail@conalt.com.br';                 // SMTP username
    $mail->Password = 'h0}qXQux(V&0';                           // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                    // TCP port to connect to

    $mail->setFrom('sendmail@conalt.com.br', 'Contato Via Web Site');
    $mail->addAddress('sac@conalt.com.br', 'Contato');     // Add a recipient
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = 'Contato via Web Site';
    $mail->Body = "<p>Abaixo Segue os dados recebidos através do formulário do website:</p>
        <table>
        <tr>
            <td><b>Nome: </b>{$_POST['nome']}</td>
        </tr>
          <tr>
            <td>
                <b>E-mail: </b> {$_POST['email']}</td>
          </tr>
          <tr>
            <td>
                <b>Telefone: </b> {$_POST['telefone']}</td>
          </tr>
          <tr>
            <td>
                <b>Mensagem: </b><br>
                {$_POST['mensagem']}
            </td>
          </tr>
    </table>";

    if(!$mail->send()) {
      echo json_encode([ 'sent' => false, 'message' => "Ocorreu um erro ao enviar." ]);
    } else {
      echo json_encode(['sent' => true]);
    }
  } catch(phpmailerException $e) {
    echo json_encode($e->errorMessage());
  } catch(Exception $e) {
    echo json_encode($e->getMessage());
  }

} else {
  echo json_encode([ 'sent' => false, 'message' => "Necessário enviar o formulário." ]);
}

