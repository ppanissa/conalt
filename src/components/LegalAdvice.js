import React from 'react';
import ScrollableAnchor from 'react-scrollable-anchor';

export default function BusinessManagement() {
  return (
    <ScrollableAnchor id="assessoria-juridica">
      <div className="legal-advice legal-advice-bg-image">
        <div className="layer"></div>
        <div className="legal-advice-box-border" data-aos="fade-right">
          <div className="box" data-aos="flip-up" data-aos-delay="400">
            <div className="box-body">
              <h2>Assessoria Jurídica</h2>
              <p>
                Nosso serviço de assessoria jurídica respalda-se no
                acompanhamento diário de empresas e pessoas físicas que possuem
                um volume considerável de operações comerciais e ações judiciais
                que exijam produção de instrumentos jurídicos pelo profissional
                do direito.
              </p>
              <p>
                O serviço de assessoria jurídica é prestado na modalidade de
                contrato mensal e é agregado com o serviço de consultoria
                jurídica que lida na prevenção e aconselhamento, enquanto a
                assessoria jurídica é mais voltada a situações consolidadas.
              </p>
              <h4>Atuamos em vários campos do direito dentre eles:</h4>
              <div className="row mb-20">
                <div className="col-6">
                  <ul>
                    <li>- DIREITO CÍVIL E RESPONSABILIDADE</li>
                    <li>- DIREITO DO CONSUMIDOR</li>
                    <li>- DIREITO PREVIDÊNCIÁRIO</li>
                    <li>- DIREITO TRABALHISTA</li>
                    <li>- PROCEDIMENTOS EXTRAJUDICIAIS</li>
                  </ul>
                </div>
                <div className="col-6">
                  <ul>
                    <li>- REVISÃO DE CONTRATOS</li>
                    <li>- ATUAÇÃO PREVENTIVA</li>
                    <li>- DIREITO EMPRESARIAL E SOCIETÁRIO</li>
                    <li>- REESTRUTURAÇÃO E RECUPERAÇÃO DE EMPRESAS</li>
                    <li>- DIREITO TRIBUTÁRIO</li>
                  </ul>
                </div>
              </div>
              <a className="button" href="#contato">
                Entre em contato
              </a>
            </div>
          </div>
        </div>
      </div>
    </ScrollableAnchor>
  );
}
