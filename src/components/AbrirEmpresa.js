import React from 'react';

export default function AbrirEmpresa() {
  return (
    <div className="abrir-empresa">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h2>VENHA ABRIR SUA EMPRESA CONOSCO!</h2>
            <div className="row">
              <div className="d-flex justify-content-center align-items-center col-lg-8 col-md-6 col-sm-12">
                <p>
                  Abertura de empresa sem complicações para você iniciar o seu
                  negócio!
                  <br />
                  Consultoria técnica especializada para dar o suporte a
                  abertura de seu CNPJ!
                  <br />
                  Verificação do melhor enquadramento tributário de sua empresa!
                </p>
              </div>
              <div className="d-flex justify-content-center align-items-center col-lg-4 col-md-6 col-sm-12 mt-sm-50">
                <div className="button" data-aos="fade-down">
                  <a href="#contato" className="btn-abrir-empresa">
                    ABRIR MINHA EMPRESA
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
