import React, { useEffect } from 'react';
import Anchor from './Anchor';
import ConaltLogo from '../assets/images/logo-conalt.svg';

export default function Navbar({ hash }) {
  const menu = [
    {
      hash: '#home',
      name: 'Home',
    },
    {
      hash: '#gestao-empresarial',
      name: 'Gestão Empresarial',
    },
    {
      hash: '#assessoria-juridica',
      name: 'Assessoria Jurídica',
    },
    {
      hash: '#servicos',
      name: 'Serviços Contábeis',
    },
    {
      hash: '#contato',
      name: 'Fale Conosco',
    },
  ];

  useEffect(() => {}, [hash]);

  return (
    <div className="app-header header--transparent sticker" id="main-menu">
      <div className="container">
        <div
          className="row justify-content-center logo-wrapper"
          data-aos="fade-up"
        >
          <img src={ConaltLogo} alt="Conalt - Logo" />
        </div>
        <div className="row align-items-center">
          <div className="col-lg-12 d-none d-lg-block">
            <div className="main wrapper" data-aos="fade-down">
              <nav>
                <ul className="main-menu">
                  <li className="brand-logo">
                    <Anchor href="#home">
                      <img src={ConaltLogo} alt="Conalt - Logo" />
                    </Anchor>
                  </li>
                  {menu.map((item, index) => (
                    <li
                      className={item.hash === hash ? 'active' : null}
                      key={index}
                    >
                      <Anchor href={item.hash}>{item.name}</Anchor>
                    </li>
                  ))}
                </ul>
                <Anchor
                  className="button-conalt"
                  href={process.env.REACT_APP_CLIENT}
                  target="_blank"
                >
                  Cliente Conalt
                </Anchor>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
