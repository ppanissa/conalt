import React from 'react';
import Anchor from './Anchor';
import LogoAltcon from '../assets/images/altcon.png';
import LogoConaltAgro from '../assets/images/conaltagro.png';

export default function Specialties() {
  return (
    <div className="specialties">
      <div className="container">
        <div className="row">
          <div className="d-flex align-items-start justify-content-center flex-column col-12 col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <h2>Outras Especialidades:</h2>
            <p>
              Contamos também com nossa diversidade de especialidade técnica
              profissional para lhe atender em outras particularidades. Saiba
              mais acessando ao lado
            </p>
          </div>
          <div className="d-flex col-12 col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div
              className="card-services"
              data-aos="fade-down"
              data-aos-offset="0"
            >
              <Anchor href="http://altcon.adm.br/" target="_blank">
                <img src={LogoAltcon} alt="Altcon" />
              </Anchor>
            </div>
            <div
              className="card-services"
              data-aos="fade-down"
              data-aos-delay="150"
              data-aos-offset="0"
            >
              <Anchor href="http://conaltagro.com.br" target="_blank">
                <img src={LogoConaltAgro} alt="Conalt Agro" />
              </Anchor>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
