import React from 'react';
import ScrollableAnchor from 'react-scrollable-anchor';
import CardBox from './CardBox';

export default function BusinessManagement() {
  return (
    <ScrollableAnchor id="gestao-empresarial">
      <div className="feature-area feature-bg-image vertical-feature">
        <div className="layer"></div>
        <div className="container">
          <div className="row">
            <div className="col-12 col-lg-6">
              <div className="card-title">
                <h2>Gestão Empresarial</h2>
                <p>
                  Nossos serviços se comprometem com o crescimento de seu
                  negócio, proporcionando um atendimento personalizado aos
                  clientes e otimizando seus resultados.
                </p>
              </div>
              <CardBox
                icon="contract"
                title="ESTUDOS E PROJETOS SÓCIO-ECONÔMICO FINANCEIRO"
                text="Apuramos a situação contábil e econômico-financeira, assim como trabalhista, comercial, fiscal da empresa-alvo para elaboração de relatórios concisos e..."
                effect="fade-right"
                more={true}
                fullText="
                Apuramos a situação contábil e econômico-financeira, assim como trabalhista, comercial, fiscal da empresa-alvo para elaboração de relatórios concisos e objetivos acerca dos elementos analisados. Viabilizando otimização e reduções de custo para sua empresa
                <br />
                <br />
                O projeto econômico financeiro é a base técnica necessária para captação de recursos de terceiros e obtenção dos incentivos fiscais concedidos pelo poder público nas esferas federal, estadual e municipal."
                delay="100"
              />
              <CardBox
                icon="ic-treinamento"
                title="TREINAMENTOS E CAPACITAÇÕES"
                text="Oferecemos cursos e treinamentos in company com a finalidade de capacitar empresários e colaboradores no empreendedorismo."
                effect="fade-right"
                delay="200"
              />
              <CardBox
                icon="ic-consultoria"
                title="CONSULTORIA TRIBUTÁRIA"
                text="Damos suporte para sua empresa e negócio a lidar com todas as modificações e legislações vigentes, através de nosso departamento fiscal, sendo tributos diretos e indiretos; trabalhistas e previdenciários, expatriados e etc."
                effect="fade-right"
                delay="250"
              />
            </div>
            <div className="col-12 col-lg-6 pt-50">
              <CardBox
                icon="ic-consultoria"
                title="CONSULTORIA TRABALHISTA E PREVIDÊNCIARIA"
                text="Elaboramos cálculos trabalhistas em todas as formas, assim como a averiguação de riscos em contencioso e impactos trabalhistas em empresas, bem como, estudos de recuperação de créditos e análise de viabilidade de desoneração da folha de pagamento."
                effect="fade-right"
                delay="300"
              />
              <CardBox
                icon="ic-viabilidade"
                title="VIABILIDADE, OTIMIZAÇÃO E REDUÇÃO DE CUSTOS"
                text="A análise de viabilidade econômica e financeira é um estudo que visa a medir ou analisar se um determinado processo dentro de um empreendimento, é viável ou não."
                effect="fade-right"
                fullText="
                A análise de viabilidade econômica e financeira é um estudo que visa a medir ou analisar se um determinado processo dentro de um empreendimento, é viável ou não.
                <br /><br />
                Em outras palavras, a análise de viabilidade econômica e financeira irá comparar os retornos que poderão ser obtidos com os investimentos demandados em processos específicos, para decidir se vale a pena ou não manter ou aprimorar tais processos.
                "
                more={true}
                delay="350"
              />
              <CardBox
                icon="ic-consultoria"
                title="CONSULTORIA FINANCEIRA E EMPRESARIAL"
                text="A Gestão Financeira eficaz necessita contar com conceitos práticos para melhor aplicação dos recursos e para isso oferecemos orientações e ferramentas necessárias..."
                fullText="A Gestão Financeira eficaz necessita contar com conceitos práticos para melhor aplicação dos recursos e para isso oferecemos orientações e ferramentas necessárias, assim como a consultoria empresarial que está ligado a prática em que o cliente se utiliza e quais as necessidades que necessitam ser atendidas."
                more={true}
                effect="fade-right"
                delay="400"
              />
            </div>
          </div>
        </div>
      </div>
    </ScrollableAnchor>
  );
}
