import React from 'react';
import Anchor from './Anchor';
import ConaltLogo from '../assets/images/logo-conalt.svg';

export default function Footer() {
  return (
    <div className="footer">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="footer-body">
              <Anchor href="#home">
                <img src={ConaltLogo} alt="Logo Conalt" />
              </Anchor>
              <p>
                J. A. Viveros Eireli. / CNPJ: 37.564.861/0001-02 | Rua das
                Garças, 1484 - Centro - Campo Grande – MS CEP: 79010-020
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
