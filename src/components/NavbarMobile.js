import React, { useState } from 'react';
import Anchor from './Anchor';

export default function NavbarMobile() {
  // Menu
  const [show, setShow] = useState(false);
  //
  const menu = [
    {
      hash: '#home',
      name: 'Home',
    },
    {
      hash: '#gestao-empresarial',
      name: 'Gestão Empresarial',
    },
    {
      hash: '#assessoria-juridica',
      name: 'Assessoria Jurídica',
    },
    {
      hash: '#servicos',
      name: 'Serviços Contábeis',
    },
    {
      hash: '#contato',
      name: 'Fale Conosco',
    },
  ];

  return (
    <>
      <button
        id="burger"
        className={
          show ? 'd-lg-none open-main-nav is-open' : 'd-lg-none open-main-nav'
        }
        onClick={() => setShow(!show)}
      >
        <span className="burger"></span>
        <span className="burger-text">Menu</span>
      </button>

      <div
        className={
          show
            ? 'mob-container show d-block d-lg-none'
            : 'mob-container hidden d-block d-lg-none'
        }
      >
        <nav className={show ? 'main-nav is-open' : 'main-nav'} id="main-nav">
          <ul>
            {menu.map((item, index) => (
              <li key={index}>
                <Anchor href={item.hash} onClick={() => setShow(!show)}>
                  {item.name}
                </Anchor>
              </li>
            ))}
            <li>
              <Anchor
                className="button-conalt"
                href={process.env.REACT_APP_CLIENT}
                target="_blank"
              >
                Cliente Conalt
              </Anchor>
            </li>
          </ul>
        </nav>
      </div>
    </>
  );
}
