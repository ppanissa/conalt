import React, { useState } from 'react';
import Anchor from './Anchor';
import PropTypes from 'prop-types';

export default function CardBox({
  icon,
  title,
  text,
  fullText,
  list,
  more,
  effect,
  delay,
}) {
  const [show, setShow] = useState(false);

  const handleClick = e => {
    setShow(!show);
  };
  return (
    <div className="card-box" data-aos={effect} data-aos-delay={delay}>
      <div className="icon">
        {icon ? (
          <img src={require(`../assets/images/icons/${icon}.svg`)} alt={icon} />
        ) : (
          ''
        )}
      </div>
      <div className="body">
        <h3>{title}</h3>
        {fullText && show ? (
          <p dangerouslySetInnerHTML={{ __html: fullText }}></p>
        ) : (
          <p dangerouslySetInnerHTML={{ __html: text }}></p>
        )}
        {list ? (
          <ul>
            {list.map((item, idx) => (
              <li key={idx}>{item}</li>
            ))}
          </ul>
        ) : (
          ''
        )}
      </div>
      {more ? (
        <Anchor onClick={handleClick} className="link">
          - {!show ? 'Leia mais' : 'Fechar'}
        </Anchor>
      ) : (
        ''
      )}
    </div>
  );
}

CardBox.defaultProps = {
  more: false,
  effect: 'fade-up',
  delay: 0,
};

CardBox.propTypes = {
  icon: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  fullText: PropTypes.string,
  list: PropTypes.array,
};
