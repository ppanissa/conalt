import React, { useState, useEffect, useRef } from 'react';
import ScrollableAnchor from 'react-scrollable-anchor';
import SweetAlert from 'sweetalert-react';
import Icon from './Icon';
import axios from 'axios';
import 'sweetalert/dist/sweetalert.css';

export default function Contact() {
  const textareaRef = useRef(null);
  const [formName, setFormName] = useState('');
  const [formEmail, setFormEmail] = useState('');
  const [formTelefone, setFormTelefone] = useState('');
  const [formMessage, setFormMessage] = useState('');
  const [btnDisabled, setBtnDisabled] = useState(false);
  const [formSubmitted, setFormSubmitted] = useState({
    show: false,
    title: '',
    text: '',
    type: 'info',
  });

  const [contact, setContact] = useState({
    phone: '',
    cell: '',
    email: '',
    address: '',
    locate: '',
  });
  useEffect(() => {
    function getEnv() {
      const phone = process.env.REACT_APP_CONTACT_PHONE;
      const cell = process.env.REACT_APP_CONTACT_CELL;
      const email = process.env.REACT_APP_CONTACT_EMAIL;
      const address = process.env.REACT_APP_CONTACT_ADDRESS;
      const locate = process.env.REACT_APP_CONTACT_CITY;

      setContact({
        phone,
        cell,
        email,
        address,
        locate,
      });
    }
    getEnv();
  }, []);

  const handleSubmit = async e => {
    e.preventDefault();
    const action = `${process.env.PUBLIC_URL}/${process.env.REACT_APP_FORM_SEND}`;
    // set disabled button send
    setBtnDisabled(true);

    try {
      if (!formName || !formEmail || !formMessage) {
        setFormSubmitted(prevState => ({
          ...prevState,
          show: true,
          type: 'error',
          title: 'Preencha os campos',
          text: 'É necessário informar os dados para enviar a mensagem.',
        }));
        return false;
      }
      // Send Message
      const form = await axios.post(action, {
        nome: formName,
        email: formEmail,
        telefone: formTelefone,
        mensagem: formMessage,
      });
      const { data } = form;
      if (data.sent) {
        // Clear
        const textarea = textareaRef;
        textarea.value = '';
        setFormName('');
        setFormEmail('');
        setFormTelefone('');
        setFormMessage('');

        setFormSubmitted(prevState => ({
          ...prevState,
          show: true,
          type: 'success',
          title: 'Mensagem Enviada',
          text: 'Em breve, entraremos em contato',
        }));
      } else {
        setFormSubmitted(prevState => ({
          ...prevState,
          show: true,
          type: 'error',
          title: 'Opsss!',
          text: 'Ocorreu um error ao enviar a mensagem',
        }));
      }
    } catch (e) {
      setFormSubmitted({
        show: true,
        type: 'error',
        title: 'Internal Error',
        text: 'Ocorreu um error no servidor!',
      });
    } finally {
      setBtnDisabled(false);
    }
  };

  return (
    <ScrollableAnchor id="contato">
      <div className="contato bg-footer">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 col-md-12 col-sm-12">
              <div className="box" data-aos="fade-up">
                <h2>Contato</h2>
                <p>
                  Para sanar suas dúvidas ou qualquer outra informação, acesse
                  nossa página de contato e conheça os meios de comunicação da
                  Conalt Centro Operacional de Negócios.
                </p>
                <ul>
                  <li>
                    <Icon icon="call-answer" />
                    {contact.phone}
                  </li>
                  <li>
                    <Icon icon="ic-cel" />
                    {contact.cell}
                  </li>
                  <li>
                    <Icon icon="ic-mail" />
                    <a href={`mailto:${contact.email}`}>{contact.email}</a>
                  </li>
                  <li>
                    <Icon icon="ic-endereco" />
                    {contact.address}
                  </li>
                  <li>
                    <Icon icon="ic-campo" />
                    {contact.locate}
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-6 col-md-12 col-sm-12">
              <div className="box" data-aos="fade-up">
                <h3>Agende um horário</h3>
                <form onSubmit={handleSubmit}>
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Seu nome"
                      onChange={e => setFormName(e.target.value)}
                      value={formName}
                      tabIndex="1"
                    />
                  </div>
                  <div className="form-group">
                    <input
                      type="email"
                      className="form-control"
                      placeholder="Seu email"
                      onChange={e => setFormEmail(e.target.value)}
                      value={formEmail}
                      tabIndex="2"
                    />
                  </div>
                  <div className="form-group">
                    <input
                      className="form-control"
                      placeholder="Seu telefone"
                      onChange={e => setFormTelefone(e.target.value)}
                      value={formTelefone}
                      tabIndex="3"
                    />
                  </div>
                  <div className="form-group">
                    <textarea
                      ref={textareaRef}
                      className="form-control form-textarea"
                      placeholder="Mensagem"
                      onChange={e => setFormMessage(e.target.value)}
                      value={formMessage}
                      tabIndex="4"
                    >
                      {formMessage}
                    </textarea>
                  </div>
                  <div className="box-submit">
                    <button
                      disabled={btnDisabled}
                      type="submit"
                      className="btn-enviar"
                      tabIndex="5"
                    >
                      Enviar
                    </button>
                  </div>
                  <SweetAlert
                    show={formSubmitted.show}
                    title={formSubmitted.title}
                    text={formSubmitted.text}
                    type={formSubmitted.type}
                    onConfirm={() => {
                      setFormSubmitted(prevState => ({
                        ...prevState,
                        show: false,
                      }));
                    }}
                    onEscapeKey={() =>
                      setFormSubmitted(prevState => ({
                        ...prevState,
                        show: false,
                      }))
                    }
                    onOutsideClick={() =>
                      setFormSubmitted(prevState => ({
                        ...prevState,
                        show: false,
                      }))
                    }
                  />
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </ScrollableAnchor>
  );
}
