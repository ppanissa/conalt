import React from 'react';
import ScrollableAnchor, { removeHash } from 'react-scrollable-anchor';

removeHash();

export default function Into() {
  return (
    <>
      <ScrollableAnchor id="home">
        <div className="slider-area bg-color bg-shape" data-bg-color="#2d3e50">
          <div className="layer"></div>
          <div className="container h-100">
            <div className="row">
              <div className="col-lg-12 h-100 d-flex align-items-center">
                <div
                  className="container-into"
                  data-aos="fade-right"
                  data-aos-offset="300"
                  data-aos-easing="ease-in-sine"
                >
                  <h1>
                    DESDE 1993 AGREGANDO VALORES PARA O
                    <br />
                    EMPREENDEDORISMO DE SUCESSO
                  </h1>
                  <p>
                    Especializada em serviços de contabilidade, consultorias
                    contábeis e assessoria financeira e jurídica, executando há
                    mais de 25 anos no segmento e contando com uma equipe
                    técnica disciplinada de Contadores, Advogados,
                    Administradores, Técnicos de Recursos Humanos. A CONALT se
                    dispõe a agregar o seu empreendedorismo como parceiro, com a
                    finalidade de auferir mais resultados e atingir o sucesso de
                    sua empresa.
                  </p>
                  <div className="button">
                    <a className="btn-into" href="#contato">
                      Conheça mais
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </ScrollableAnchor>
    </>
  );
}
