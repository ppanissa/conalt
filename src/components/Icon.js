import React from 'react';

export default function Icon({ icon }) {
  return (
    <img src={require(`./../assets/images/icons/${icon}.svg`)} alt={icon} />
  );
}
