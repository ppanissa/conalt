import React from 'react';
import Anchor from './Anchor';
import InstagramLogo from '../assets/images/icons/insta.svg';
import FacebookLogo from '../assets/images/icons/facebook-1.svg';
// import LinkedInLogo from '../assets/images/icons/linkedin.svg';

export default function SocialMedia() {
  return (
    <div className="social-media">
      <div className="container">
        <div className="row">
          <div className="col-12 d-flex justify-content-center align-items-center ">
            <div className="media" data-aos="fade-up">
              <Anchor
                href="https://www.instagram.com/conalt.contabilidade"
                target="_blank"
              >
                <img src={InstagramLogo} alt="Instagram" />
                <p>Instagram</p>
              </Anchor>
            </div>
            <div className="media" data-aos="fade-up">
              <Anchor
                href="https://www.facebook.com/conalt.contabilidade"
                target="_blank"
              >
                <img src={FacebookLogo} alt="Facebook" />
                <p>Facebook</p>
              </Anchor>
            </div>
            {/* <div className="media" data-aos="fade-up">
              <Anchor
                href="https://www.linkedin.com/company/conalt"
                target="_blank"
              >
                <img src={LinkedInLogo} alt="LinkedIn" />
                <p>LinkedIn</p>
              </Anchor>
            </div> */}
          </div>
        </div>
      </div>
    </div>
  );
}
