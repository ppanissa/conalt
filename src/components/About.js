import React, { useState } from 'react';
import ReactPlayer from 'react-player';

export default function About() {
  const [opts] = useState({
    youtube: {
      playerVars: {
        controls: 0,
        fs: 0,
        disablekb: 1,
        modestbranding: 1,
        showinfo: 0,
        rel: 0,
      },
    },
  });

  return (
    <div className="section sobre-nos">
      <div className="container">
        <div className="row">
          <div
            className="col-12 col-md-12 col-lg-5"
            data-aos="fade-right"
            data-aos-duration="3000"
          >
            <div className="about">
              <h2>Sobre nós</h2>
              <p>
                Estamos há mais de 25 anos executando atividades de pertinência
                Contábil e Fiscal, assim como realizando consultorias com uma
                equipe técnica especializada e profissional. Somos uma empresa
                com foco em resultados e redução de impostos priorizando nosso
                rigor técnico e compromisso.
              </p>
              <p>
                Nossa expertise é aprimorar soluções e estratégias que garantam
                a competitividade de nossos clientes frente aos desafios do
                empreendedorismo garantindo-lhes o sucesso.
                <br />
                <strong>Clique no vídeo e saiba mais</strong>
              </p>
            </div>
          </div>
          <div className="col-12 col-md-12 col-lg-7">
            <div
              className="video-wrapper"
              data-aos="fade-right"
              data-aos-duration="2500"
              data-aos-delay="500"
            >
              <ReactPlayer
                url="https://www.youtube.com/watch?v=qFgII2ogKCM"
                config={opts}
                playing
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
