import React from 'react';
import ScrollableAnchor from 'react-scrollable-anchor';
import CardBox from './CardBox';

export default function Services() {
  return (
    <ScrollableAnchor id="servicos">
      <div className="services">
        <div className="container">
          <div className="row">
            <div className="header col-lg-12 col-md-12 coll-sm-12">
              <div className="row">
                <div className="col-lg-5 col-md-5 col-sm-12">
                  <h2 className="title">Serviços Contábeis</h2>
                </div>
                <div className="offset-lg-1 offset-md-1 col-lg-6 col-md-6 col-sm-12">
                  <p className="sub-title">
                    Com a equipe da Conalt Centro Operacional de Negócios
                    altamente capacitado e integrado à ciência contábil,
                    proporcionamos um atendimento personalizado colocando as
                    responsabilidades tributárias de sua empresa em dia.
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-6 col-md-12">
              <CardBox
                icon="ic-contabi"
                title="CONTABILIDADE"
                text="Oferecemos Suporte e Assessoria Contábil visando a eficiência da sua empresa, para os serviços:"
                list={[
                  'Elaboração de relatórios distintivo, observando balanço patrimonial e gerencial, demonstrativo de resultados, índices financeiros e econômicos;',
                  'Balancetes mensais e demonstrações contábeis;',
                  'Balanços trimetrais e demonstrações contábeis;',
                  'Escrituração Fiscal, Emissão de Guias Federais, Estaduais, Municipais e Taxas;',
                  'Gias, Sintegras, DMS;',
                  'Declarações de Imposto de Renda pessoas júrídica e física;',
                  'DACON, DCTF, Relatório de Exportação RE;',
                  'Levantamento de Débitos, Certidões Negativas do INSS, FGTS e demais órgãos federais;',
                  'Serviços terceirizados com pessoas capacitadas dentro da sua empresa;',
                ]}
                effect="fade-right"
                delay="100"
              />
              <CardBox
                icon="ic-tributacao"
                title="TRIBUTAÇÃO E DIAGNÓSTICO FISCAL"
                text="Oferecemos Assessoria em Tributação e Diagnóstico Fiscal visando a eficiência da sua empresa, para os serviços:"
                list={[
                  'Solução de contingências fiscais;',
                  'Exame e Avaliação dos procedimentos tributários conforme legislação vigente;',
                  'Auditoria nas Folhas de Pagamentos dos últimos 05 anos;',
                  'Auditoria nos pagamentos PIS/PASEP, COFINS, IPI, ICMS, IRPJ e CSSL;',
                  'Estudo de viabilidade de recebimento de tributos pagos indevidamente;',
                  'Encaminhamento para recebimento das Verbas Indenizatórias do INSS;',
                ]}
                effect="fade-right"
                delay="100"
              />
              <CardBox
                icon="ic-gestão"
                title="GESTÃO DE PESSOAS"
                text="Conte com nosso Recursos Humanos otimizado para as necessidades do mercado. Serviços oferecidos:"
                list={[
                  'Recursos Humanos;',
                  'Treinamento, capacitação e desenvolvimento de funcionários e empresários;',
                  'Cursos de Capacitação, Administrativo, Suporte e Financeiro;',
                  'Palestras empresariais e motivacionais;',
                  'Pesquisas de Satisfação junto aos clientes;',
                ]}
                effect="fade-right"
                delay="100"
              />
            </div>
            <div className="col-lg-6 col-md-12">
              <CardBox
                icon="ic-auditoria"
                title="AUDITORIA"
                text="Auditoria e Revisão das demonstrações contábeis, assim como Processos e Controles Internos para buscar a eficiência de sua empresa. Oferecemos para todos os nossos clientes Análise Empresarial a cada 3 meses contemplando índices de crescimento através dos serviços:"
                list={[
                  'Auditoria Interna e de Gestão a cada 6 meses;',
                  'Auditoria das Demonstrações Financeiras;',
                  'Inventário Físico de Estoque e Ativo Fixo;',
                  'Relatórios Gerenciais;',
                  'Análise de Balanços e Demonstrações;',
                  'Conciliações das Contas Contábeis;',
                ]}
                effect="fade-right"
                delay="100"
              />
              <CardBox
                icon="ic-departamento"
                title="DEPARTAMENTO PESSOAL"
                text="Nosso Departamento Pessoal conta com técnicos capacitados visando a eficiência da sua empresa, oferecendo os serviços:"
                list={[
                  'Folha de Pagamento;',
                  'Holerites;',
                  'Cálculos Trabalhistas;',
                  'Controle de Férias;',
                  'RAIS;',
                  'Declarações;',
                  'Homologações Trabalhistas;',
                  'Rescisões e Admissões;',
                ]}
                effect="fade-right"
                delay="100"
              />
              <CardBox
                icon="ic-departamento"
                title="APURAÇÃO DE HAVERES"
                text="Realização de Apuração de Haveres nos casos de dissoluções societárias, com a finalidade de analisar e apurar, as cotas societárias que cabem a cada sócio."
                effect="fade-right"
                delay="100"
              />
              <CardBox
                icon="ic-departamento"
                title="PRESTAÇÃO DE CONTAS"
                text="Oferecemos auxílio em prestação de Contas visando a eficiência da sua empresa, em:"
                list={[
                  'Ações bancárias em geral',
                  'Prestações de Contas condominiais;',
                  'Prestações de Contas em Administração de Negócios;',
                  'Prestações de Contas de órgãos públicos.',
                ]}
                effect="fade-right"
                delay="100"
              />
            </div>
          </div>
        </div>
      </div>
    </ScrollableAnchor>
  );
}
