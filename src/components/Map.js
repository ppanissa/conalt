import React, { useState, useEffect } from 'react';
import Iframe from 'react-iframe';

export default function Map() {
  const [map, setMap] = useState('');

  useEffect(() => {
    function getEnv() {
      const url = process.env.REACT_APP_MAP_URL;
      setMap(url);
    }

    getEnv();
  }, []);
  return (
    <div className="map" data-aos="zoom-in">
      <Iframe src={map} frameborder="0" allowfullscreen="" />
    </div>
  );
}
