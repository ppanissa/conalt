import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Anchor from './Anchor';

export default function Whatsapp({ disabled }) {
  const [link, setLink] = useState('#');

  useEffect(() => {
    function loadWhatsappInfo() {
      const number = process.env.REACT_APP_WHATSAPP;
      let url = 'web';
      if (
        /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
          navigator.userAgent
        )
      ) {
        url = 'api';
      }
      const link = `https://${url}.whatsapp.com/send?phone=${number}&text=Conalt&source=&data=`;
      setLink(link);
    }

    loadWhatsappInfo();
  }, []);

  return !disabled ? (
    <div className="whatsapp-container">
      <Anchor href={link} target="_blank" rel="noopener noreferrer" />
    </div>
  ) : (
    ''
  );
}

Whatsapp.defaultProps = {
  disabled: false,
};

Whatsapp.propTypes = {
  disabled: PropTypes.bool,
};
