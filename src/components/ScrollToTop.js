import React from 'react';
import ArrowUpIcon from '../assets/images/icons/arrow-up.svg';
import Anchor from './Anchor';

export default function ScrollToTop({ scroll }) {
  return (
    <>
      {scroll ? (
        <div id="scroll-to" data-aos="fade-up">
          <Anchor href="#home">
            <img src={ArrowUpIcon} alt="Scroll To Top" />
          </Anchor>
        </div>
      ) : (
        ''
      )}
    </>
  );
}
