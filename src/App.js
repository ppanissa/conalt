import React, { useState, useEffect } from 'react';
import { configureAnchors } from 'react-scrollable-anchor';

import Whatsapp from './components/Whatsapp';
import NavMobile from './components/NavbarMobile';
import Navbar from './components/Navbar';
import Into from './components/Into';
import AbrirEmpresa from './components/AbrirEmpresa';
import About from './components/About';
import Specialties from './components/Specialties';
import BusinessManagement from './components/BusinessManagement';
import LegalAdvice from './components/LegalAdvice';
import Services from './components/Services';
import Contact from './components/Contact';
import SocialMedia from './components/SocialMedia';
import Map from './components/Map';
import Footer from './components/Footer';
import ScrollToTop from './components/ScrollToTop';

configureAnchors({
  offset: -100,
  scrollDuration: 800,
});

function App() {
  const [scroll, setScroll] = useState(false);
  const [hash, setHash] = useState('#home');

  useEffect(() => {
    const navbar = document.getElementById('main-menu');

    window.onscroll = () => {
      // Set Hash
      setHash(window.location.hash);

      if (window.pageYOffset >= 500) {
        setScroll(true);
      } else {
        setScroll(false);
      }

      if (window.innerWidth > 991) {
        if (window.pageYOffset >= 80) {
          navbar.classList.add('stick');
        } else {
          navbar.classList.remove('stick');
        }
      }
    };
  }, []);

  return (
    <>
      {/* Whatsapp */}
      <Whatsapp disabled={true} />
      {/* Mobile Nav */}
      <NavMobile />
      {/* Nav */}
      <Navbar hash={hash} />
      {/* Home */}
      <Into />
      {/* Abrir Empresa */}
      <AbrirEmpresa />
      {/* Sobre Nós */}
      <About />
      {/* Especialidades */}
      <Specialties />
      {/* Gestão Empresárial */}
      <BusinessManagement />
      {/* Asessoria Jurídica */}
      <LegalAdvice />
      {/* Serviços */}
      <Services />
      {/* Contato */}
      <Contact />
      {/* Midias Sociais */}
      <SocialMedia />
      {/* Map */}
      <Map />
      {/* Footer */}
      <Footer />
      <ScrollToTop scroll={scroll} />
    </>
  );
}

export default App;
