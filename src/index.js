/**
 * Polyfill for IE
 */
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
/**
 * Initialize
 */
import React from 'react';
import ReactDOM from 'react-dom';
import Aos from 'aos';
/**
 * App
 */
import Routes from './routes';
import * as serviceWorker from './serviceWorker';
/**
 * Styles Import
 */
import 'aos/dist/aos.css';
import './assets/scss/conalt.scss';
// Initialized AOS
Aos.init();

ReactDOM.render(<Routes />, document.getElementById('root'));

serviceWorker.unregister();
